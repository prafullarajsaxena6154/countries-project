import React from 'react'
import Home from './components/home-page'
import { Route, Routes } from 'react-router-dom'
import CountryDetailsLink from './components/Country-Details-Link'
const App = () => {
  return (
    <Routes>
      <Route path='/' element={<Home />}/>
      <Route path='/:id' element={<CountryDetailsLink />}/>
      <Route path='*' element={<Error />}/>
    </Routes>
  )
}

export default App