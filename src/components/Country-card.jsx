import React from "react";
import { Link } from 'react-router-dom';

const CountryCard = ({ id,flagUrl, name, region, capital, population,subregion,darkMode }) => {
  
  return (<Link to={`/${id}`}>
    <div id={darkMode ? "darkmode" : null} className="card">
      <img className="flag" src={flagUrl}></img>
      <div id={darkMode ? "darkmode" : null}className="details">
        <div className="name">
          <h4 id={darkMode ? "darkmode" : null}>{name}</h4>
        </div>
        <div className="population">Population: {population}</div>
        <div className="region">Region: {region}</div>
        <div className="capital">Capital: {capital}</div>
        <div className="subregion">Sub-Region: {subregion}</div>
      </div>
    </div>
    </Link>
  );
};

export default CountryCard;
