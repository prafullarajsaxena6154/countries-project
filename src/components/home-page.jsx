import { useState, useEffect, useContext } from "react";
import "../App.css";
import { Mode } from "./Theme";
import Header from "./Header";
import CountryCard from "./Country-card";
import NavBar from "./Nav-Bar";

let sendingData = null;
function Home() {
  const { darkMode } = useContext(Mode);

  const [dataCountry, setDataCountry] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [regionValue, setRegionValue] = useState("");
  const [subRegionValue, setSubRegionValue] = useState("");
  const [sortBy, setSortBy] = useState("");
  const [isLoaded, setLoaded] = useState(false);
  const [error, setError] = useState("");
  useEffect(() => {
    fetch("https://restcountries.com/v3.1/all")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setDataCountry(data);
        setLoaded(true);
      })
      .catch((err) => {
        setError(err.message);
      });
  }, []);

  function handleSearch(searchData) {
    setSearchValue(searchData);
  }
  function handleRegion(regionData) {
    setRegionValue(regionData);
    setSubRegionValue("");
  }
  function handleSubRegion(subRegionData) {
    setSubRegionValue(subRegionData);
  }

  let searchResult = dataCountry.filter((country) => {
    return (
      country.region.includes(regionValue) &&
      country.name.common.toLowerCase().includes(searchValue) &&
      country.subregion &&
      country.subregion.includes(subRegionValue)
    );
  });

  let subRegionArray = dataCountry.reduce((subRegionArray, country) => {
    if (
      !subRegionArray.includes(country.subregion) &&
      country.region.includes(regionValue) &&
      country.name.common.toLowerCase().includes(searchValue) &&
      country.subregion !== undefined
    ) {
      return [...subRegionArray, country.subregion];
    } else {
      return subRegionArray;
    }
  }, []);

  function handleSort(sortBy) {
    setSortBy(sortBy);
    if (sortBy == "asc-pop") {
      searchResult = dataCountry.sort((a, b) => {
        return a.population - b.population;
      });
    } else if (sortBy == "des-pop") {
      searchResult = dataCountry.sort((b, a) => {
        return a.population - b.population;
      });
    } else if (sortBy == "asc-area") {
      searchResult = dataCountry.sort((a, b) => {
        return a.area - b.area;
      });
    } else if (sortBy == "des-area") {
      searchResult = dataCountry.sort((b, a) => {
        return a.area - b.area;
      });
    } else {
      window.location.reload();
    }
  }

  if (!isLoaded) {
    sendingData = <h1>Loading Countries Data...</h1>;
  }
  if (error != "") {
    sendingData = <h1>{error}</h1>;
  } else {
    if (searchResult.length > 0) {
      sendingData = searchResult.map((country) => {
        return (
          <CountryCard
            key={country.cca3}
            id={country.cca3}
            population={country.population}
            flagUrl={country.flags.png}
            name={country.name.common}
            region={country.region}
            capital={country.capital}
            subregion={country.subregion}
            darkMode={darkMode}
          />
        );
      });
    } else if (searchResult.length === 0 && isLoaded) {
      sendingData = <h1>No countries found!</h1>;
    }
  }

  return (
    <>
      <div className={darkMode ? "darkBackground" : null}>
        <Header />
        <NavBar
          onChangeSearch={handleSearch}
          onChangeRegion={handleRegion}
          subRegionArray={subRegionArray}
          onChangeSubRegion={handleSubRegion}
          onChangeSort={handleSort}
          darkMode={darkMode}
        />
        <div id={darkMode ? "darkBackground" : null} className="main-container">
          {sendingData}
        </div>
      </div>
    </>
  );
}

export default Home;
