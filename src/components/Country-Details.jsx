import React, { useContext } from "react";
import { Mode } from "./Theme";
import Header from "./Header";
import "./Country-Details.css";
import { Link } from "react-router-dom";
import BorderCountryDetails from "./Border-Country-Details";
const CountryDetails = ({ countryData }) => {
  const { darkMode } = useContext(Mode);
  return (
    <>
      <Header />
      <div className={!darkMode ? "detail-page" : "darkBackground detail-page"}>
        <div id="left-container">
          <button className={darkMode ? "darkmode back" : "back"}>
            <Link to="/">
              <i class="fa-solid fa-arrow-left fa-sm"></i> Back to Home
            </Link>
          </button>

          <img className="flag-L" src={countryData.flags.svg}></img>
        </div>
        <div id="right-container">
          <div className="nameDiv">
            <div className="name">
              <h2>{countryData.name.common}</h2>
            </div>
          </div>
          <div className="detailsL">
            <div className="nativeName">
              <b>Native Name:</b>{" "}
              {Object.values(countryData.name.nativeName)[0].common}
            </div>
            <div className="population">
              <b>Population: </b>
              {countryData.population}
            </div>
            <div className="region">
              <b>Region:</b> {countryData.region}
            </div>
            <div className="subregion">
              <b>Sub-Region:</b> {countryData.subregion}
            </div>
            <div className="capital">
              <b>Capital:</b> {countryData.capital}
            </div>
            <div className="topLevelDomain">
              <b>Top Level Domain:</b> {countryData.tld}
            </div>
            <div className="currency">
              <b>Currencies:</b> {Object.values(countryData.currencies)[0].name}
            </div>
            <div className="language">
              <b>Languages:</b>{" "}
              {Object.values(countryData.languages).join(", ")}
            </div>
          </div>
          <div className="borderCountries">
            <b>Border Countries:</b>
            <BorderCountryDetails borders={countryData.borders} />
          </div>
        </div>
      </div>
    </>
  );
};

export default CountryDetails;
