import React, { useContext } from "react";
import { Mode } from "./Theme";

const Header = () => {
  const { darkMode, setDarkMode } = useContext(Mode);
  function onButtonPress() {
    setDarkMode((darkMode) => !darkMode);
  }
  return (
    <div id={darkMode ? "darkmode" : null} className="header">
      <h2>Where in the world?</h2>
      <button
        onClick={onButtonPress}
        className={darkMode ? "darkmode darkbutton" : "darkbutton"}
      >
        <i id="moon" className="fa-solid fa-moon fa-sm"></i>Dark Mode
      </button>
    </div>
  );
};

export default Header;
