import React from "react";

const SubRegion = ({ subRegion }) => {
  return <option value={subRegion}>{subRegion}</option>;
};

export default SubRegion;
