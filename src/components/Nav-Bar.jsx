import React from "react";
import SubRegion from "./Sub-Region";

const NavBar = ({
  onChangeSearch,
  onChangeRegion,
  subRegionArray,
  onChangeSubRegion,
  onChangeSort,
  darkMode
}) => {
  return (
    <div className="navBar">
      <div id={darkMode ? "darkmode" : null} className="search-bar">
        <i id={darkMode ? "darkmode" : 'search-icon'} className="fas fa-search search-icon"></i>
        <input
          type="text"
          id={darkMode ? "darkmode" : null}
          className="search-box"
          placeholder="Search country by name"
          onChange={(e) => onChangeSearch(e.target.value)}
        />
      </div>
      <label htmlFor="sort"></label>
      <select
        onChange={(e) => onChangeSort(e.target.value)}
        name="sort-select"
        id="sort"
        className={darkMode ? "darkmode" : null}
      >
        <option value="">Sort By</option>
        <option value="asc-pop">Ascending Population</option>
        <option value="des-pop">Decending Population</option>
        <option value="asc-area">Ascending Area</option>
        <option value="des-area">Decending Area</option>
      </select>
      <label htmlFor="region-select"></label>
      <select
        onChange={(e) => onChangeRegion(e.target.value)}
        name="region"
        className={darkMode ? "darkmode" : null}
        id="region-select"
      >
        <option value="">Filter By Region/All</option>
        <option value="Africa">Africa</option>
        <option value="Americas">America</option>
        <option value="Asia">Asia</option>
        <option value="Europe">Europe</option>
        <option value="Oceania">Oceania</option>
      </select>

      <label htmlFor="sub-region-select"></label>
      <select
        onChange={(e) => onChangeSubRegion(e.target.value)}
        name="sub-region"
        id="sub-region-select"
        className={darkMode ? "darkmode" : null}
      >
        <option value="">Filter by Sub-Region/All</option>
        {subRegionArray.map((element) => {
          return <SubRegion key={element} subRegion={element} />;
        })}
      </select>
    </div>
  );
};
export default NavBar;
