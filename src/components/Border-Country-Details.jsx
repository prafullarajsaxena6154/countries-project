import React from "react";
import { Mode } from "./Theme";
import { useEffect, useState, useContext } from "react";
import { Link } from "react-router-dom";
const BorderCountryDetails = ({ borders }) => {
  const { darkMode } = useContext(Mode);

  if (borders === undefined) {
    return <div>none.</div>;
  }
  const [countriesData, setCountriesData] = useState([]);

  useEffect(() => {
    fetch("https://restcountries.com/v3.1/all")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setCountriesData(
          data.filter((ele) => {
            return borders.includes(ele.cca3);
          })
        );
        console.log(countriesData);
      });
  }, [countriesData]);

  if (countriesData.length > 0) {
    let borderData = "";

    borderData = countriesData.map((ele, index) => {
      console.log(index);
      return (
        <Link key={ele.cca3} to={`/${ele.cca3}`}>
          <button className={darkMode ? "darkmode" : null}>
            {ele.name.common}
          </button>
        </Link>
      );
    });
    return <div>{borderData}</div>;
  } else {
    return <div>Loading..</div>;
  }
};

export default BorderCountryDetails;
