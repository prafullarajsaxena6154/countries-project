import React from "react";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import CountryDetails from "./Country-Details";
const CountryDetailsLink = () => {
  const { id } = useParams();
  const [cardData, setCardData] = useState("");
  let pathNew = "https://restcountries.com/v3.1/alpha/";
  pathNew += id;
  useEffect(() => {
    fetch(pathNew)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setCardData(data[0]);
      })
      .catch((err) => {
        setError(err.message);
      });
  }, [id]);
  return cardData != "" && <CountryDetails countryData={cardData} />;
};

export default CountryDetailsLink;
